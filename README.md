@nfi/tuplemap
======
[![npm version][npm-image]][npm-url]
[![pipeline status][pipeline-image]][pipeline-url]
[![coverage status][coverage-image]][coverage-url]

Map implementation using N-tuples as multi-dimensional keys; matches
the interface and behaviour of the ES2015 Map wherever possible.

## Installation
```
npm install @nfi/tuplemap
```

## Documentation
API documentation is available [here](https://cptpackrat.gitlab.io/tuplemap).

### Iteration Order
TupleMaps do not guarantee iteration over keys in insertion order like Maps do;
iteration order is _consistent_ for any given set of keys, but not easily predictable.

### Deletion Strategy
By default, TupleMaps clean up empty/unneeded internal structures on every key deletion.
The `delete` method has an optional parameter, `prune`, that can be used to disable this
behaviour. This strategy can increase performance for collections with high turnover on
a consistent set of keys, but  _must_ be supplemented with periodic calls to `prune` or
`clear` to manually clean up unused structures or they will leak memory over time.

[npm-image]: https://img.shields.io/npm/v/@nfi/tuplemap.svg
[npm-url]: https://www.npmjs.com/package/@nfi/tuplemap
[pipeline-image]: https://gitlab.com/cptpackrat/tuplemap/badges/master/pipeline.svg
[pipeline-url]: https://gitlab.com/cptpackrat/tuplemap/commits/master
[coverage-image]: https://gitlab.com/cptpackrat/tuplemap/badges/master/coverage.svg
[coverage-url]: https://gitlab.com/cptpackrat/tuplemap/commits/master
