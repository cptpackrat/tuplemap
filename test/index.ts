import { inspect } from 'util'
import { TupleMap } from '../src/index'
import { TupleMap2 } from '../src/tuplemap2'
import { TupleMap3 } from '../src/tuplemap3'
import { TupleMap4 } from '../src/tuplemap4'
import { TupleMapN } from '../src/tuplemapN'

describe('TupleMap', () => {
  it('uses TupleMapN for N = 1 (why tho)', () => {
    expect(new TupleMap<any, any>(1)).toBeInstanceOf(TupleMapN)
  })
  it('uses TupleMap2 for N = 2', () => {
    expect(new TupleMap<any, any>(2)).toBeInstanceOf(TupleMap2)
  })
  it('uses TupleMap3 for N = 3', () => {
    expect(new TupleMap<any, any>(3)).toBeInstanceOf(TupleMap3)
  })
  it('uses TupleMap4 for N = 4', () => {
    expect(new TupleMap<any, any>(4)).toBeInstanceOf(TupleMap4)
  })
  it('uses TupleMapN for N > 4', () => {
    expect(new TupleMap<any, any>(5)).toBeInstanceOf(TupleMapN)
    expect(new TupleMap<any, any>(6)).toBeInstanceOf(TupleMapN)
    expect(new TupleMap<any, any>(7)).toBeInstanceOf(TupleMapN)
    expect(new TupleMap<any, any>(8)).toBeInstanceOf(TupleMapN)
  })
  it('rejects invalid N', () => {
    expect(() => new TupleMap<any, any>(0)).toThrow(/invalid N.+0/)
    expect(() => new TupleMap<any, any>(0.5)).toThrow(/invalid N.+0.5/)
    expect(() => new TupleMap<any, any>(4.5)).toThrow(/invalid N.+4.5/)
    expect(() => new TupleMap<any, any>(-5)).toThrow(/invalid N.+-5/)
    expect(() => new TupleMap<any, any>(false)).toThrow(/invalid N.+false/)
    expect(() => new TupleMap<any, any>('tuesday')).toThrow(/invalid N.+tuesday/)
  })
})

/* eslint-disable-next-line @typescript-eslint/prefer-function-type */
describe.each<[name: string, num: number, TupleMap: { new (num?: number): TupleMap<string[], string> }]>([
  ['TupleMap2', 2, TupleMap2],
  ['TupleMap3', 3, TupleMap3],
  ['TupleMap4', 4, TupleMap4],
  ['TupleMapN (for N = 1) (why tho)', 1, TupleMapN],
  ['TupleMapN (for N = 2)', 2, TupleMapN],
  ['TupleMapN (for N = 3)', 3, TupleMapN],
  ['TupleMapN (for N = 4)', 4, TupleMapN],
  ['TupleMapN (for N = 5)', 5, TupleMapN]
])('%s', (_, num, TupleMap) => {
  const [kv1, kv2] = genKVs(num, 40)
  const map = new TupleMap(num)
  it('behaves correctly when empty', () => {
    for (const [key] of kv1) {
      expect(map.has(key)).toBe(false)
      expect(map.get(key)).toBe(undefined)
      expect(map.delete(key)).toBe(false)
      expect(map.size).toBe(0)
    }
  })
  it('inserts keys correctly', () => {
    for (const [key, val] of kv1.slice(0, 20)) {
      expect(map.set(key, val)).toBe(map)
    }
    for (const [key, val] of kv1.slice(0, 20)) {
      expect(map.has(key)).toBe(true)
      expect(map.get(key)).toBe(val)
    }
    for (const [key] of kv1.slice(20)) {
      expect(map.has(key)).toBe(false)
      expect(map.get(key)).toBe(undefined)
    }
    expect(map.size).toBe(20)
  })
  it('updates keys correctly', () => {
    for (const [key, val] of kv2.slice(10, 30)) {
      expect(map.set(key, val)).toBe(map)
    }
    for (const [key, val] of kv1.slice(0, 10)) {
      expect(map.has(key)).toBe(true)
      expect(map.get(key)).toBe(val)
    }
    for (const [key, val] of kv2.slice(10, 30)) {
      expect(map.has(key)).toBe(true)
      expect(map.get(key)).toBe(val)
    }
    for (const [key] of kv1.slice(30)) {
      expect(map.has(key)).toBe(false)
      expect(map.get(key)).toBe(undefined)
    }
    expect(map.size).toBe(30)
  })
  it('deletes keys correctly', () => {
    for (const [key] of kv1.slice(25, 30)) {
      expect(map.delete(key)).toBe(true)
    }
    for (const [key] of kv1.slice(30, 35)) {
      expect(map.delete(key)).toBe(false)
    }
    for (const [key, val] of kv1.slice(0, 10)) {
      expect(map.has(key)).toBe(true)
      expect(map.get(key)).toBe(val)
    }
    for (const [key, val] of kv2.slice(10, 25)) {
      expect(map.has(key)).toBe(true)
      expect(map.get(key)).toBe(val)
    }
    for (const [key] of kv1.slice(25)) {
      expect(map.has(key)).toBe(false)
      expect(map.get(key)).toBe(undefined)
    }
    expect(map.size).toBe(25)
  })
  it('deletes all keys correctly', () => {
    map.clear()
    for (const [key] of kv1) {
      expect(map.has(key)).toBe(false)
      expect(map.get(key)).toBe(undefined)
    }
    expect(map.size).toBe(0)
  })
  it('iterates over keys correctly', () => {
    const map = new TupleMap(num)
    const exp = new Set(kv1.slice(0, 20).map((kv) => kv[0]))
    expect([...map.keys()]).toStrictEqual([])
    kv1.slice(0, 20).forEach((kv) => map.set(...kv))
    kv2.slice(10, 30).forEach((kv) => map.set(...kv))
    kv2.slice(20).forEach((kv) => map.delete(kv[0]))
    expect(new Set(map.keys())).toStrictEqual(exp)
  })
  it('iterates over values correctly', () => {
    const map = new TupleMap(num)
    const exp = new Set([
      ...kv1.slice(0, 10),
      ...kv2.slice(10, 20)
    ].map((kv) => kv[1]))
    expect([...map.values()]).toStrictEqual([])
    kv1.slice(0, 20).forEach((kv) => map.set(...kv))
    kv2.slice(10, 30).forEach((kv) => map.set(...kv))
    kv2.slice(20).forEach((kv) => map.delete(kv[0]))
    expect(new Set(map.values())).toStrictEqual(exp)
  })
  it('iterates over entries correctly', () => {
    const map = new TupleMap(num)
    const exp = new Set([
      ...kv1.slice(0, 10),
      ...kv2.slice(10, 20)
    ])
    expect([...map]).toStrictEqual([])
    expect([...map.entries()]).toStrictEqual([])
    kv1.slice(0, 20).forEach((kv) => map.set(...kv))
    kv2.slice(10, 30).forEach((kv) => map.set(...kv))
    kv2.slice(20).forEach((kv) => map.delete(kv[0]))
    const forEach = new Set<any>()
    map.forEach((v, k, m) => {
      expect(m).toBe(map)
      forEach.add([k, v])
    })
    expect(forEach).toStrictEqual(exp)
    expect(new Set(map)).toStrictEqual(exp)
    expect(new Set(map.entries())).toStrictEqual(exp)
  })
  it('handles false and nullable values correctly', () => {
    const map = new (TupleMap as {
      /* eslint-disable-next-line @typescript-eslint/prefer-function-type */
      new (num?: number): TupleMap<string[], any>
    })(num)
    const [k1, k2, k3, k4] = kv1.map(([k]) => k)
    expect(map.set(k1, null)).toBe(map)
    expect(map.set(k2, false)).toBe(map)
    expect(map.set(k3, undefined)).toBe(map)
    expect(map.get(k1)).toBe(null)
    expect(map.get(k2)).toBe(false)
    expect(map.get(k3)).toBe(undefined)
    expect(map.get(k4)).toBe(undefined)
    expect(map.has(k1)).toBe(true)
    expect(map.has(k2)).toBe(true)
    expect(map.has(k3)).toBe(true)
    expect(map.has(k4)).toBe(false)
    expect(new Set([...map.entries()])).toStrictEqual(new Set([
      [k1, null],
      [k2, false],
      [k3, undefined]
    ]))
  })
  it('behaves correctly internally', () => {
    const map = new TupleMap(num)
    expect(deepCount(num, map)).toBe(expCount(num, 0))
    for (const [key, val] of kv1.slice(0, 20)) {
      map.set(key, val)
    }
    expect(deepCount(num, map)).toBe(expCount(num, 20))
    for (const [key, val] of kv2.slice(10)) {
      map.set(key, val)
    }
    expect(deepCount(num, map)).toBe(expCount(num, 40))
    for (const [key] of kv1.slice(20, 30)) {
      map.delete(key)
    }
    expect(deepCount(num, map)).toBe(expCount(num, 30))
    map.clear()
    expect(deepCount(num, map)).toBe(1)
    for (const [key, val] of genOverlapKVs(num)) {
      map.set(key, val)
    }
    expect(deepCount(num, map)).toBe(expOverlapCount(num))
    for (const [key] of genOverlapKVs(num)) {
      map.delete(key, false)
    }
    expect(deepCount(num, map)).toBe(expOverlapCount(num, true))
    map.prune()
    expect(deepCount(num, map)).toBe(expCount(num, 0))
  })
  it('represents itself with dignity', () => {
    const map = new TupleMap(num)
    const obj = { map }
    /* eslint-disable-next-line @typescript-eslint/no-base-to-string */
    expect(map.toString()).toBe(`[object TupleMap(${num})]`)
    expect(inspect(obj, { colors: false, depth: 0 })).toBe(`{ map: TupleMap(${num}) }`)
    expect(inspect(obj, { colors: false, depth: 1 })).toBe(`{ map: TupleMap(${num}) {} }`)
    for (const [key, val] of kv1) {
      map.set(key, val)
    }
    const d0 = inspect(obj, { colors: false, depth: 0 })
    const d1 = inspect(obj, { colors: false, depth: 1 })
    expect(d0).toBe(`{ map: TupleMap(${num}) }`)
    expect(d1).toContain(`map: TupleMap(${num}) {`)
    for (const [key, val] of kv1) {
      for (const k of key) {
        expect(d1).toContain(k)
      }
      expect(d1).toContain(`=> '${val}'`)
    }
  })
})

function genKVs (num: number, count: number): [
  kv1: Array<[key: string[], val: string]>,
  kv2: Array<[key: string[], val: string]>
] {
  const kv1: Array<[string[], string]> = []
  const kv2: Array<[string[], string]> = []
  for (let idx = 0; count > 0; count--, idx++) {
    const key: string[] = []
    for (let n = 0; n < num; n++) {
      key.push(`k${n}-${idx}`)
    }
    kv1.push([key, `v1-${idx}`])
    kv2.push([key, `v2-${idx}`])
  }
  return [kv1, kv2]
}

function genOverlapKVs (num: number): Array<[key: string[], val: string]> {
  const kv: Array<[string[], string]> = []
  for (let i = 0; i < num; i++) {
    const key: string[] = []
    for (let n = 0; n < num; n++) {
      key.push(`k${n}-${Math.min(n, i)}`)
    }
    kv.push([key, `v-${i}`])
  }
  return kv
}

function deepCount (num: number, obj: TupleMap<any, any>): number {
  function count (num: number, map: Map<any, Map<any, any>>): number {
    return num === 1
      ? map.size + 1
      : [...map.values()].reduce((sum, map) => sum + count(num - 1, map), 1)
  }
  const map = (obj as any).m ?? (obj as any).m0
  expect(map).toBeInstanceOf(Map)
  return count(num, map)
}

function expCount (num: number, count: number): number {
  return 1 + num * count
}

function expOverlapCount (num: number, lazy: boolean = false): number {
  if (lazy) {
    num--
  }
  return num > 1
    ? num + expOverlapCount(num - 1)
    : num + 1
}
