import { inspectMap } from './inspect'
import { inspect, InspectOptions } from 'util'

/* eslint-disable @typescript-eslint/lines-between-class-members */

/** Specific implementation for 2-tuple keys. */
export class TupleMap2<K extends [any, any], V> {
  private readonly m0 = new Map<K[0], Map<K[1], V>>()
  private sz?: number = 0
  get size (): number {
    if (this.sz === undefined) {
      this.sz = 0
      for (const m1 of this.m0.values()) {
        this.sz += m1.size
      }
    }
    return this.sz
  }
  has (k: K): boolean {
    return this.m0.get(k[0])?.has(k[1]) ?? false
  }
  get (k: K): V | undefined {
    return this.m0.get(k[0])?.get(k[1])
  }
  set (k: K, v: V): this {
    const m1 = this.m0.get(k[0])
    if (m1 !== undefined) {
      m1.set(k[1], v)
    } else {
      this.m0.set(k[0], new Map().set(k[1], v))
    }
    this.sz = undefined
    return this
  }
  delete (k: K, prune: boolean = true): boolean {
    let rv = false
    const m1 = this.m0.get(k[0])
    if (m1 !== undefined) {
      rv = m1.delete(k[1])
      if (prune && m1.size === 0) {
        this.m0.delete(k[0])
      }
      if (rv && this.sz !== undefined) {
        this.sz--
      }
    }
    return rv
  }
  clear (): void {
    this.m0.clear()
    this.sz = 0
  }
  prune (): void {
    for (const [k0, m1] of this.m0.entries()) {
      if (m1.size === 0) {
        this.m0.delete(k0)
      }
    }
  }
  forEach (fn: (val: V, key: K, map: this) => void): void {
    for (const [key, val] of this.entries()) {
      fn(val, key, this)
    }
  }
  * keys (): IterableIterator<K> {
    for (const [k0, m1] of this.m0) {
      for (const k1 of m1.keys()) {
        yield [k0, k1] as any
      }
    }
  }
  * values (): IterableIterator<V> {
    for (const m1 of this.m0.values()) {
      yield * m1.values()
    }
  }
  * entries (): IterableIterator<[K, V]> {
    for (const [k0, m1] of this.m0) {
      for (const [k1, v] of m1) {
        yield [[k0, k1] as any, v]
      }
    }
  }
  [Symbol.iterator] (): IterableIterator<[K, V]> {
    return this.entries()
  }
  get [Symbol.toStringTag] (): string {
    return 'TupleMap(2)'
  }
  [inspect.custom] (depth: number, opts: InspectOptions, inspect?: any): string {
    return inspectMap(this, 2, depth, opts, inspect)
  }
}
