import { TupleMap } from './index'
import { inspect, InspectOptions } from 'util'

export function inspectMap (
  map: TupleMap<any, any>,
  num: number,
  depth: number,
  opts: InspectOptions,
  insp = inspect
): string {
  if (depth < 0) {
    return `TupleMap(${num})`
  }
  opts = { ...opts, depth }
  const pairs = [...map]
  return pairs.length === 0
    ? `TupleMap(${num}) {}`
    : `TupleMap(${num}) {${pairs
        .map(([key, val]) => `\n  ${insp(key, opts)} => ${insp(val, opts)}`)
        .join(',')}\n}`
}
