import { inspectMap } from './inspect'
import { inspect, InspectOptions } from 'util'

/* eslint-disable @typescript-eslint/lines-between-class-members */

/** Specific implementation for 4-tuple keys. */
export class TupleMap4<K extends [any, any, any, any], V> {
  private readonly m0 = new Map<K[0], Map<K[1], Map<K[2], Map<K[3], V>>>>()
  private sz?: number = 0
  get size (): number {
    if (this.sz === undefined) {
      this.sz = 0
      for (const m1 of this.m0.values()) {
        for (const m2 of m1.values()) {
          for (const m3 of m2.values()) {
            this.sz += m3.size
          }
        }
      }
    }
    return this.sz
  }
  has (k: K): boolean {
    return this.m0.get(k[0])?.get(k[1])?.get(k[2])?.has(k[3]) ?? false
  }
  get (k: K): V | undefined {
    return this.m0.get(k[0])?.get(k[1])?.get(k[2])?.get(k[3])
  }
  set (k: K, v: V): this {
    const m1 = this.m0.get(k[0])
    if (m1 !== undefined) {
      const m2 = m1.get(k[1])
      if (m2 !== undefined) {
        const m3 = m2.get(k[2])
        if (m3 !== undefined) {
          m3.set(k[3], v)
        } else {
          m2.set(k[2], new Map().set(k[3], v))
        }
      } else {
        m1.set(k[1], new Map().set(k[2], new Map().set(k[3], v)))
      }
    } else {
      this.m0.set(k[0], new Map().set(k[1], new Map().set(k[2], new Map().set(k[3], v))))
    }
    this.sz = undefined
    return this
  }
  delete (k: K, prune: boolean = true): boolean {
    let rv = false
    const m1 = this.m0.get(k[0])
    if (m1 !== undefined) {
      const m2 = m1.get(k[1])
      if (m2 !== undefined) {
        const m3 = m2.get(k[2])
        if (m3 !== undefined) {
          rv = m3.delete(k[3])
          if (prune && m3.size === 0) {
            m2.delete(k[2])
            if (m2.size === 0) {
              m1.delete(k[1])
              if (m1.size === 0) {
                this.m0.delete(k[0])
              }
            }
          }
          if (rv && this.sz !== undefined) {
            this.sz--
          }
        }
      }
    }
    return rv
  }
  clear (): void {
    this.m0.clear()
    this.sz = 0
  }
  prune (): void {
    for (const [k0, m1] of this.m0.entries()) {
      for (const [k1, m2] of m1.entries()) {
        for (const [k2, m3] of m2.entries()) {
          if (m3.size === 0) {
            m2.delete(k2)
          }
        }
        if (m2.size === 0) {
          m1.delete(k1)
        }
      }
      if (m1.size === 0) {
        this.m0.delete(k0)
      }
    }
  }
  forEach (fn: (val: V, key: K, map: this) => void): void {
    for (const [key, val] of this.entries()) {
      fn(val, key, this)
    }
  }
  * keys (): IterableIterator<K> {
    for (const [k0, m1] of this.m0) {
      for (const [k1, m2] of m1) {
        for (const [k2, m3] of m2) {
          for (const k3 of m3.keys()) {
            yield [k0, k1, k2, k3] as any
          }
        }
      }
    }
  }
  * values (): IterableIterator<V> {
    for (const m1 of this.m0.values()) {
      for (const m2 of m1.values()) {
        for (const m3 of m2.values()) {
          yield * m3.values()
        }
      }
    }
  }
  * entries (): IterableIterator<[K, V]> {
    for (const [k0, m1] of this.m0) {
      for (const [k1, m2] of m1) {
        for (const [k2, m3] of m2) {
          for (const [k3, v] of m3) {
            yield [[k0, k1, k2, k3] as any, v]
          }
        }
      }
    }
  }
  [Symbol.iterator] (): IterableIterator<[K, V]> {
    return this.entries()
  }
  get [Symbol.toStringTag] (): string {
    return 'TupleMap(4)'
  }
  [inspect.custom] (depth: number, opts: InspectOptions, inspect?: any): string {
    return inspectMap(this, 4, depth, opts, inspect)
  }
}
