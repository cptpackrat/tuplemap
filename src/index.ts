import { TupleMap2 } from './tuplemap2'
import { TupleMap3 } from './tuplemap3'
import { TupleMap4 } from './tuplemap4'
import { TupleMapN } from './tuplemapN'

/* eslint-disable @typescript-eslint/method-signature-style */
/* eslint-disable @typescript-eslint/no-extraneous-class */

export interface TupleMap<K extends any[], V> {
  /** Returns the current number of keys in the map. */
  readonly size: number
  /** Returns `true` if the given key exists in the map. */
  has (key: K): boolean
  /** Returns the value associated with the given key if it exists,
    * or `undefined` if it does not. */
  get (key: K): V | undefined
  /** Sets the value associated with the given key; returns the map. */
  set (key: K, val: V): this
  /** Removes the given key and its associated value from the map if present;
    * returns `true` if the key existed and has been removed, `false` otherwise.
    * @param prune True by default; if false, internal structures will be left in
    *              place for re-use. This can increase performance for collections
    *              with high turnover on a consistent set of keys, but must be
    *              supplemented with periodic calls to `prune` or `clear` or it
    *              constitutes a memory leak. */
  delete (key: K, prune?: boolean): boolean
  /** Removes all keys and their associated values from the map,
    * and prunes all internal structures. */
  clear (): void
  /** Clean up any internal structures left after lazy deletion. */
  prune (): void
  /** Calls the given callback function for each key in the map. */
  forEach (fn: (val: V, key: K, map: this) => void): void
  /** Returns an iterator that yields each key in the map. */
  keys (): IterableIterator<K>
  /** Returns an iterator that yields each value in the map. */
  values (): IterableIterator<V>
  /** Returns an iterator that yields a `[key, value]` tuple for each key in the map. */
  entries (): IterableIterator<[K, V]>
  /** Returns an iterator that yields a `[key, value]` tuple for each key in the map. */
  [Symbol.iterator](): IterableIterator<[K, V]>
}

/** Map implementation using N-tuples as multi-dimensional keys; matches
  * the interface and behaviour of the ES2015 Map wherever possible.
  * @template K Key tuple type.
  * @template V Value type.
  * @template N Length of K; i.e. number of key dimensions for this map.
  * @note **Iteration Order:**
  *   TupleMaps do not guarantee iteration over keys in insertion
  *   order like Maps do; iteration order is _consistent_ for any
  *   given set of keys, but not easily predictable.
  * @note **Deletion Strategy:**
  *   By default, TupleMaps clean up empty/unneeded internal structures on
  *   every key deletion. The `delete` method has an optional parameter,
  *   `prune`, that can be used to disable this behaviour. This strategy can
  *   increase performance for collections with high turnover on a consistent
  *   set of keys, but  _must_ be supplemented with periodic calls to `prune`
  *   or `clear` to manually clean up unused structures or they will leak
  *   memory over time. */
export class TupleMap<K extends any[], V, N = K['length']> {
  /** @param n Number of key dimensions for this map. */
  constructor (n: N)
  constructor (n: any) {
    switch (n) {
      case 2: return new TupleMap2<any, V>()
      case 3: return new TupleMap3<any, V>()
      case 4: return new TupleMap4<any, V>()
    }
    return new TupleMapN<K, V>(n)
  }
}
