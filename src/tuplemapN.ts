import { inspectMap } from './inspect'
import { inspect, InspectOptions } from 'util'

/* eslint-disable @typescript-eslint/lines-between-class-members */

/** Generic implementation for N-tuple keys. */
export class TupleMapN<K extends any[], V, N = K['length']> {
  private readonly n: number
  private readonly m = new Map<any, any>()
  private sz?: number = 0
  constructor (n: N)
  constructor (n: any) {
    if (n < 1 || !Number.isInteger(n)) {
      /* eslint-disable-next-line @typescript-eslint/restrict-template-expressions */
      throw new RangeError(`invalid N; expected positive integer, got ${String(n)}`)
    }
    this.n = n
  }
  get size (): number {
    if (this.sz === undefined) {
      const count = (i: number, m: Map<any, any>): number => {
        if (i === this.n - 1) {
          return m.size
        }
        let sz = 0
        for (const mN of m.values()) {
          sz += count(i + 1, mN)
        }
        return sz
      }
      this.sz = count(0, this.m)
    }
    return this.sz
  }
  has (k: K, i: number = 0, m: Map<any, any> = this.m): boolean {
    return m.has(k[i])
      ? i < this.n - 1
        ? this.has(k, i + 1, m.get(k[i]))
        : true
      : false
  }
  get (k: K, i: number = 0, m: Map<any, any> = this.m): V | undefined {
    return m.has(k[i])
      ? i < this.n - 1
        ? this.get(k, i + 1, m.get(k[i]))
        : m.get(k[i])
      : undefined
  }
  set (k: K, v: V, i: number = 0, m: Map<any, any> = this.m): this {
    if (i === this.n - 1) {
      m.set(k[i], v)
      this.sz = undefined
    } else {
      let mN = m.get(k[i])
      if (mN === undefined) {
        m.set(k[i], mN = new Map())
      }
      this.set(k, v, i + 1, mN)
    }
    return this
  }
  delete (k: K, prune: boolean = true, i: number = 0, m: Map<any, any> = this.m): boolean {
    let rv = false
    if (i === this.n - 1) {
      rv = m.delete(k[i])
      if (rv && this.sz !== undefined) {
        this.sz--
      }
    } else {
      const mN = m.get(k[i])
      if (mN !== undefined) {
        rv = this.delete(k, prune, i + 1, mN)
        if (prune && mN.size === 0) {
          m.delete(k[i])
        }
      }
    }
    return rv
  }
  clear (): void {
    this.m.clear()
    this.sz = 0
  }
  prune (i: number = 0, m: Map<any, any> = this.m): void {
    for (const [k, mN] of m.entries()) {
      if (i < this.n - 1) {
        this.prune(i + 1, mN)
      }
      if (mN.size === 0) {
        m.delete(k)
      }
    }
  }
  forEach (fn: (val: V, key: K, map: this) => void): void {
    for (const [key, val] of this.entries()) {
      fn(val, key, this)
    }
  }
  * keys (): IterableIterator<K> {
    const n = this.n
    function * iterate (k: any[], m: Map<any, any>): Generator<any, void, undefined> {
      if (k.length < n - 1) {
        for (const [kN, mN] of m) {
          yield * iterate([...k, kN], mN)
        }
      } else {
        for (const kN of m.keys()) {
          yield [...k, kN]
        }
      }
    }
    yield * iterate([], this.m)
  }
  * values (): IterableIterator<V> {
    const n = this.n
    function * iterate (i: number, m: Map<any, any>): Generator<V, void, undefined> {
      if (i < n - 1) {
        for (const mN of m.values()) {
          yield * iterate(i + 1, mN)
        }
      } else {
        for (const v of m.values()) {
          yield v
        }
      }
    }
    yield * iterate(0, this.m)
  }
  * entries (): IterableIterator<[K, V]> {
    const n = this.n
    function * iterate (k: any[], m: Map<any, any>): Generator<[any, V], void, undefined> {
      if (k.length < n - 1) {
        for (const [kN, mN] of m) {
          yield * iterate([...k, kN], mN)
        }
      } else {
        for (const [kN, v] of m) {
          yield [[...k, kN], v]
        }
      }
    }
    yield * iterate([], this.m)
  }
  [Symbol.iterator] (): IterableIterator<[K, V]> {
    return this.entries()
  }
  get [Symbol.toStringTag] (): string {
    return `TupleMap(${this.n})`
  }
  [inspect.custom] (depth: number, opts: InspectOptions, inspect?: any): string {
    return inspectMap(this, this.n, depth, opts, inspect)
  }
}
