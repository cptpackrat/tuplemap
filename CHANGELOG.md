# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.0.1](https://gitlab.com/cptpackrat/tuplemap/compare/v1.0.0...v1.0.1) (2023-07-21)


### Bug Fixes

* Fixed 2/3/4-tuple implementations not handling nullable values correctly. ([330a602](https://gitlab.com/cptpackrat/tuplemap/commit/330a602829bb1abd4fa3843940fef294ed85caa1))

## 1.0.0 (2022-05-29)

* Initial release.
